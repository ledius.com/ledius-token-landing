const ctx = document.getElementById('myChart').getContext('2d');
Chart.register(ChartDataLabels);
var myChart = new Chart(ctx, {
    type: 'pie',
    options: {
        plugins: {
            tooltip: false,
            datalabels: {
                formatter: function(value, context) {
                    return ''
                },
                color: 'white',
                font: {
                    family: 'Montserrat, sans-serif',
                    size: window.innerWidth > 1000 ? 20 : 14
                }
            }
        }
    },
    data: {
        datasets: [{
            data: [50, 20, 10, 5],
            backgroundColor: [
                '#522BC0',
                '#2434CD',
                '#4A59E9',
                '#7883E7',
            ],
            borderColor: [
                'transparent'
            ],
            borderRadius: 5
        }]
    },
});