const gulp = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const sourcemaps = require('gulp-sourcemaps')
const rename = require('gulp-rename')
const autoprefixer = require('gulp-autoprefixer')

gulp.task('sass-compile', function() {
    return gulp.src('./scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
        outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(rename('index.min.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css'))
})

gulp.task('watch', function() {
    gulp.watch('./scss/**/*.scss', gulp.series('sass-compile'))
})