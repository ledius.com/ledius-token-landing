
const burgerClickHandler = () => {
    const button = document.querySelector('header > .header-right .burger-btn')
    const burgerMenu = document.querySelector('.burger-menu')
    button.classList.contains('isOpen') ? (button.classList.remove('isOpen'), document.documentElement.style.overflowY = 'unset') : (button.classList.add('isOpen'), document.documentElement.style.overflowY = 'hidden')
    burgerMenu.classList.contains('isOpen') ? burgerMenu.classList.remove('isOpen') : burgerMenu.classList.add('isOpen')
}

const switchLangClickHandler = () => {
    if (window.innerWidth <= 643) {
        const button = document.querySelector('.burger-menu button.lang-switch ')
        button.classList.contains('isOpen') ? button.classList.remove('isOpen') : button.classList.add('isOpen')
    } else {
        const button = document.querySelector('.header-right li button.lang-switch')
        button.classList.contains('isOpen') ? button.classList.remove('isOpen') : button.classList.add('isOpen')
    }
}

const switchLangHandler = (e, setLang = true) => {
    let a 
    if (window.innerWidth >= 644) {
        a = document.querySelector('.header-right .lang-switch-flag')
    } else {
        a = document.querySelector('.burger-menu .lang-switch-flag')
    }
    if (e == 'usa') {
        a.children[0].attributes['onclick'].value = 'switchLangHandler("usa")'
        a.children[1].attributes['onclick'].value = 'switchLangHandler("rus")'
        a.children[0].classList.remove('lang-switch-flag-rus')
        a.children[0].classList.add('lang-switch-flag-usa')
        a.children[1].classList.remove('lang-switch-flag-usa')
        a.children[1].classList.add('lang-switch-flag-rus')
        localStorage.setItem('lang', 'usa')
        setLang ? setUsaLang() : ''
    } else {
        a.children[1].attributes['onclick'].value = 'switchLangHandler("usa")'
        a.children[0].attributes['onclick'].value = 'switchLangHandler("rus")'
        a.children[1].classList.remove('lang-switch-flag-rus')
        a.children[1].classList.add('lang-switch-flag-usa')
        a.children[0].classList.remove('lang-switch-flag-usa')
        a.children[0].classList.add('lang-switch-flag-rus')
        localStorage.setItem('lang', 'rus')
        setLang ? setRusLang() : ''
    }
}

const setUsaLang = () => {
    setLangFunction('en')
    switchLangHandler('usa', setLang = false)
}

const setRusLang = () => {
    setLangFunction('ru')
    switchLangHandler('rus', setLang = false)
}

const setLangFunction = (lang) => {
    const menus = document.querySelectorAll('#navigation')
    const joinUsBtns = document.querySelectorAll('#joinUs')
    const intro = document.querySelector('.intro-content')
    const firstSection = document.querySelector('.first-section')
    const secondSection = document.querySelector('.second-section')
    const thirdSection = document.querySelector('.third-section')
    const economics = document.querySelector('.economics')
    const roadmap = document.querySelector('.roadmap')
    const links = document.querySelector('#projects')
    menus.forEach(menu => {
        menu.children[0].children[0].innerHTML = content.menu.main[lang]
        menu.children[1].children[0].innerHTML = content.menu.adv[lang]
        menu.children[2].children[0].innerHTML = content.menu.economics[lang]
        menu.children[4].children[0].innerHTML = content.menu.projects[lang]
    })
    joinUsBtns.forEach(btn => {
        btn.innerHTML = content.joinUsBtn[lang]
    })
    intro.children[0].innerHTML = content.intro.bigText[lang]
    intro.children[1].innerHTML = content.intro.smallText[lang]
    firstSection.children[0].children[0].innerHTML = content.firstSection.bigText[lang]
    firstSection.children[0].children[1].innerHTML = content.firstSection.smallText[lang]
    secondSection.children[0].children[0].innerHTML = content.secondSection.bigText[lang]
    secondSection.children[0].children[1].innerHTML = content.secondSection.smallText[lang]
    thirdSection.children[0].children[0].innerHTML = content.thirdSection.bigText[lang]
    thirdSection.children[0].children[1].innerHTML = content.thirdSection.smallText[lang]
    economics.children[0].children[0].innerHTML = content.economics.label[lang]
    economics.children[0].children[1].children[0].innerHTML = content.economics.firstBlock.label[lang]
    economics.children[0].children[1].children[1].innerHTML = content.economics.firstBlock.info[lang]
    economics.children[0].children[1].children[2].innerHTML = content.economics.secondBlock.label[lang]
    economics.children[0].children[1].children[3].innerHTML = content.economics.secondBlock.info[lang]
    economics.children[0].children[1].children[4].innerHTML = content.economics.thirdBlock.label[lang]
    economics.children[0].children[1].children[5].innerHTML = content.economics.thirdBlock.info[lang]
    economics.children[0].children[1].children[6].innerHTML = content.economics.fourthBlock.label[lang]
    economics.children[0].children[1].children[7].innerHTML = content.economics.fourthBlock.info[lang]
    roadmap.children[0].innerHTML = content.roadMap.label[lang]
    roadmap.children[1].children[1].children[0].children[1].innerHTML = content.roadMap.firstBlock[lang]
    roadmap.children[1].children[1].children[1].children[1].innerHTML = content.roadMap.secondBlock[lang]
    roadmap.children[1].children[1].children[2].children[1].innerHTML = content.roadMap.thirdBlock[lang]
    roadmap.children[1].children[1].children[3].children[1].innerHTML = content.roadMap.fourthBlock[lang]
    links.children[0].children[1].innerHTML = content.projects.first.label[lang]
    links.children[0].children[2].innerHTML = content.projects.first.info[lang]
    links.children[1].children[1].innerHTML = content.projects.second.label[lang]
    links.children[1].children[2].innerHTML = content.projects.second.info[lang]
}

const initLang = () => {
    if (!localStorage.getItem('lang')) {
        window.navigator.language != 'ru-RU' ? setUsaLang() : setRusLang()
        return
    }
    localStorage.getItem('lang') == 'rus' ? setRusLang() : setUsaLang()
}
initLang()

const windowScrollTo = (id, px = 0) => {
    window.scrollTo({top: document.querySelector(`#${id}`).offsetTop - (window.innerWidth >= 644 ? 20 + px : px), behavior: "smooth"})
}

window.onresize = function () {
    let button 
    !button ? button = document.querySelector('header > .header-right .burger-btn') : ''
    setImagesQuality()
    if (button.classList.contains('isOpen')) {
        burgerClickHandler()
    } 
}

// ALERT - SOME DEPRECATED
const setImagesQuality = () => {
    if (window.innerWidth >= 600) {
//         document.querySelector('#third-section-img').src = './imgs/thirdSectionXL.png'
//         document.querySelector('#second-section-img').src = './imgs/secondSectionXL.png'
//         document.querySelector('#first-section-img').src = './imgs/firstSectionXL.png'
        document.querySelector('#links-first-img').src = './imgs/linksFirstXL.png'
        document.querySelector('#links-second-img').src = './imgs/linksSecondXL.png'
    } else {
//         document.querySelector('#third-section-img').src = './imgs/thirdSection.png'
//         document.querySelector('#second-section-img').src = './imgs/secondSection.png'
//         document.querySelector('#first-section-img').src = './imgs/firstSection.png'
        document.querySelector('#links-first-img').src = './imgs/linksFirstXL.png'
        document.querySelector('#links-second-img').src = './imgs/linksSecondXL.png'
    }
}
setImagesQuality()

window.onscroll = () => {
    if (window.pageYOffset > 20) {
        makeHeaderNontransparent()
    } else {
        makeHeaderTransparent()
    }
}

window.onresize = () => {
    if (window.pageYOffset > 20) {
        makeHeaderNontransparent()
    } else {
        makeHeaderTransparent()
    }
}

const makeHeaderNontransparent = () => {
    if (window.innerWidth >= 644) {
        document.querySelector('.header-wrap').style = 'background: rgba(31, 31, 31, .93); backdrop-filter: blur(4px);'
    } else {
        document.querySelector('header').style = 'background: rgba(31, 31, 31, .93); backdrop-filter: blur(4px);'
    }
}

const makeHeaderTransparent = () => {
    if (window.innerWidth >= 644) {
        document.querySelector('.header-wrap').style = 'background: transparent; backdrop-filter: blur(0px)'
    } else {
        document.querySelector('header').style = 'background: transparent; backdrop-filter: blur(0px)'
    }
}