const content = {
    menu: {
        main: {
            ru: "Главная",
            en: 'Home'
        },
        adv: {
            ru: 'Информация',
            en: 'About'
        },
        economics: {
            ru: 'Показатели',
            en: 'Economics'
        },
        projects: {
            ru: 'Приложение',
            en: 'Projects'
        },
    },
    joinUsBtn: {
        ru: 'Присоединиться',
        en: 'Join us'
    },
    intro: {
        bigText: {
            ru: 'Ledius',
            en: 'Ledius'
        },
        smallText: {
            ru: 'Платформа цифровых финансовых активов',
            en: 'The world\'s first decentralized commercial corporation'
        }
    },
    firstSection: {
        bigText: {
            ru: 'ЦФА',
            en: 'DFA'
        },
        smallText: {
            ru: 'Активы, обязательства, металлы, товары - все чем владеет компания вы можете в это инвестировать',
            en: 'Any owner of LDS tokens is automatically the owner of the company\'s shares, and has indisputable rights. The percentage of ownership determines the opportunities that the owner has in participating in the life of the company, and gives the right to vote'
        }
    },
    secondSection: {
        bigText: {
            ru: 'Высокий потенциал',
            en: 'Resolve conflicts by Arbiter'
        },
        smallText: {
            ru: 'Выпустить ЦФА может компания любых размеров - инвестируй в ЦФА обладающие высоким потенциалом роста и доходности',
            en: 'Any participant can become an arbiter. When initiating a decision to accept an arbiter, the candidate is nominated by the initiator of the decision (the participant can nominate himself), the entire price for this decision goes to the candidate considered as an arbiter. It is impossible to create a decision on the acceptance of a judge with an existing arbiter.'
        }
    },
    thirdSection: {
        bigText: {
            ru: 'Торговля 24/7',
            en: 'Lending program'
        },
        smallText: {
            ru: 'В отличий от биржи ЦФА платформа работает круглосуточно, торгуй в любое удобное для себя время',
            en: 'When replenishing the deposit, LDS will be blocked for a period of 2 months, during which time you will be credited from 5% to 1% of the replenishment amount. The amount of accrued interest depends on the remaining funds allocated for the deposit'
        }
    },
    economics: {
        label: {
            ru: 'Показатели нашей платформы',
            en: 'Ledius <strong>Exchange</strong> Economics'
        },
        firstBlock: {
            label: {
                ru: '210',
                en: '20%'
            },
            info: {
                ru: 'ЦФА Выпущено',
                en: 'Public sale of courses'
            }
        },
        secondBlock: {
            label: {
                ru: '1.5 млн ₽',
                en: '50%'
            },
            info: {
                ru: 'Объем выпусков',
                en: 'Shared among founders and partners'
            }
        },
        thirdBlock: {
            label: {
                ru: '100 тыс ₽',
                en: '20%'
            },
            info: {
                ru: 'Выплачено дивидендов через ЦФА',
                en: 'Deposit programm'
            }
        },
        fourthBlock: {
            label: {
                ru: '0.01%',
                en: '10%'
            },
            info: {
                ru: 'Комиссия за торговлю',
                en: 'Marketing spending'
            }
        },
    },
    roadMap: {
        label: {
            ru: 'Инвестировать в <strong>ЦФА</strong>',
            en: 'Roadmap <strong>2022</strong>'
        },
        firstBlock: {
            ru: 'Отправить заявку на регистрацию на платформе ЦФА',
            en: 'Develop public exchange to release tokens'
        },
        secondBlock: {
            ru: 'Пополнить счёт на сумму которую хотите инвестировать',
            en: 'Develop education platform & integrate with LDS'
        },
        thirdBlock: {
            ru: 'Выбрать ЦФА с доходностью которая вам интересна',
            en: 'Develop solutions platform for control company lifecycle'
        },
        fourthBlock: {
            ru: 'Получать прибыль от владения ЦФА и с разницы продаж',
            en: 'Start beta version of solutions platform and receive user feedback'
        },
    },
    projects: {
        first: {
            label: {
                ru: 'Приложение',
                en: 'ledius.ru'
            },
            info: {
                ru: 'Скачайте мобильное приложение в RUStore и управляйте своими ЦФА из любой точки мира',
                en: 'Education ledius platform for sell courses. Each payment transactions used LDS tokens'
            }
        },
        second: {
            label: {
                ru: 'WEB-Версия',
                en: 'exchange.ledius.ru'
            },
            info: {
                ru: 'Получите удовольствие от торговли ЦФА в полноэкранной версии приложения',
                en: 'LDS Exchange to public sale tokens. Each user can buy LDS for FIAT money'
            }
        }
    }
}
